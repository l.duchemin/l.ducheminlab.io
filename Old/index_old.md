---
pagetitle: Laurent Duchemin's Webpage
---

-------------
<div class="navbar">
[Home](index.html) -- [Section 1](section1.html) -- [Publications](publications.html)
</div>
-------------

-------------

Laurent Duchemin
-----------------

<img src="resources/portrait_small.jpg" id="profile_pic"/>
<!--<img src="resources/profile_pic.png" id="profile_pic"/>-->

<!--| Your title-->
<!--| [Your instution](http://yourinstitute.edu)-->
<!--| E-mail: somename at somewhere -->
<!--| Office: building, room-->
<!--| Phone: number-->

<!--\ -->

I am an Assistant Professor at <a href="http://www.univ-amu.fr">Aix-Marseille Universit&eacute;</a>, 
	and my research activity is carried out at the <a href="http://www.irphe.fr">IRPHE</a> Institute in Marseille. 
	My research mainly deals with theoretical and numerical problems involving free surface flows. 
	Usually, my approach consists in simplifying the equations in order to solve them with light fluid dynamics codes, 
	like boundary integral methods, and confront theoretical models to the numerics. 
	Recently, I have been also interested in the stability of numerical methods and I wrote a paper on this subject 
	with Jens Eggers. If you want more details about my recent research activity, please follow the <a href="research.html">Research</a> link on top of this page. 
	<!--     My main research interests are : Free surface flows, slow viscous flows, non-linear fluid dynamics and computational fluid dynamics. -->
      As an Assistant Professor, I spend nearly half of the year teaching various courses. 
      At the moment, my lectures include Fluid Mechanics for the third (L3) and fourth (M1) year at the university, 
      and Statistical Mechanics for the fourth year. 






------------

[Section1](section1.html) **~** [Section2](section2.html) **~** [Section 3](section3.html)

------------
