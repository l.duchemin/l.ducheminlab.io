---
pagetitle: Laurent Duchemin's Webpage
navbar_pubs: true
---

## Preprints

-   MARS : A Method for the Adaptive Removal of Stiffness in PDEs, L.
    Duchemin & J. Eggers, submitted to JCP
    ([PDF](../Papers/Stiff.pdf)).
-   Fingering instability in adhesion fronts, M. L’Estimé, L.
    Duchemin, E. Reyssat, J. Bico, submitted to JFM
    ([PDF](../Papers/Article_tartines2.pdf)).

## Reprints

-   The shaping of plant axes and crowns through tropisms and
    elasticity: an example of morphogenetic plasticity beyond the shoot
    apical meristem
    ([PDF](../Papers/mbbde.pdf)) B.
    Moulia, E. Badel, R. Bastien, L. Duchemin and C. Eloy, [New
    Phytologist](https://nph.onlinelibrary.wiley.com/doi/full/10.1111/nph.17913),
    233, 6, 2354–2379, 2022, Wiley Online Library.
-   Dimple drainage before the coalescence of a droplet deposited on a
    smooth substrate
    ([PDF](https://blog.espci.fr/duchemin/files/2020/10/Published_version_11_10_2020_corrected.pdf)) L.
    Duchemin, C. Josserand, [Proceedings of the National Academy of
    Sciences](https://www.pnas.org/content/117/34/20416.short) 117 (34),
    20416-20422.
-   Bistability in Rayleigh-Bénard convection with a melting boundary
    ([PDF](https://blog.espci.fr/duchemin/files/2020/10/PhysRevFluids.5.023501.pdf)) J.
    Purseed, B. Favier, L. Duchemin, E.W. Hester, Physical Review Fluids
    5 (2), 023501.
-   Rayleigh–Bénard convection with a melting boundary
    ([PDF](https://blog.espci.fr/duchemin/files/2020/10/RB1.pdf)) B.
    Favier, J. Purseed, L. Duchemin, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    858, 437–473.
-   Tree crowns grow into self-similar shapes controlled by gravity and
    light
    sensing ([PDF](https://blog.espci.fr/duchemin/files/2020/10/debm.pdf)) L.
    Duchemin, C. Eloy, E. Badel & B. Moulia, *[J. R. Soc.
    Interface](http://rsif.royalsocietypublishing.org/)*, 15: 20170976
-   The diffusive sheet method for scalar
    mixing ([PDF](https://www.irphe.fr/~duchemin/PDF/DSM.pdf)) D.
    Martinez-Ruiz, P. Meunier, B. Favier, L. Duchemin & E. Villermaux,
    2018, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    837, 230–257.
-   Impact on floating
    membranes ([PDF](https://www.irphe.fr/~duchemin/PDF/vd.pdf)) N.
    Vandenberghe & L. Duchemin, 2016, *[Physical Review
    E.](http://journals.aps.org/pre/abstract/10.1103/PhysRevE.93.052801)*,
    vol. 93, 052801.
-   Self-similar impulsive capillary waves on a
    ligament ([PDF](https://www.irphe.fr/~duchemin/PDF/dlvv.pdf)) L.
    Duchemin, S. Le Dizès, L. Vincent & E. Villermaux, 2015, *[Phys.
    Fluids](http://scitation.aip.org/phf/)*, vol. 27, 051704.
-   Forced dynamics of a short viscous liquid
    bridge ([PDF](https://www.irphe.fr/~duchemin/PDF/vdl.pdf)) L.
    Vincent, L. Duchemin & S. Le Dizès, 2014, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    761, 220–240.
-   Impact dynamics for a floating elastic
    membrane ([PDF](https://www.irphe.fr/~duchemin/PDF/dv.pdf)) L.
    Duchemin & N. Vandenberghe, 2014, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    756, 544–554.
-   Two dimensional Leidenfrost Droplets in a Hele Shaw
    Cell ([PDF](https://www.irphe.fr/~duchemin/PDF/cfcrdp.pdf)) F.
    Celestini, T. Frisch, A. Cohen, C. Raufaste, L. Duchemin & Y.
    Pomeau, 2014, *[Phys. Fluids](http://scitation.aip.org/phf/)*, vol.
    26, 032103.
-   Remnants from fast liquid
    withdrawal ([PDF](https://www.irphe.fr/~duchemin/PDF/vdv.pdf)) L.
    Vincent, L. Duchemin & E. Villermaux, 2014, *[Phys.
    Fluids](http://scitation.aip.org/phf/)*, vol. 26, 031701.
-   The Explicit-Implicit-Null method: removing the numerical
    instability of
    PDEs ([PDF](https://www.irphe.fr/~duchemin/PDF/de.pdf)) L. Duchemin
    & J. Eggers, 2014, *[J. Comput.
    Phys.](http://http//www.sciencedirect.com/science/journal/00219991)*,
    vol. 263, 37–52.
-   Rarefied gas correction for the bubble entrapment singularity in
    drop impacts ([PDF](https://www.irphe.fr/~duchemin/PDF/dj2.pdf)) L.
    Duchemin & C. Josserand, 2012, *[C. R.
    Mecanique](http://www.sciencedirect.com/science/journal/16310721)*,
    vol. 340, 797–803.
-   Asymptotic behavior of a retracting two-dimensional fluid
    sheet ([PDF](https://www.irphe.fr/~duchemin/PDF/gadj.pdf))  
    L. Gordillo, G. Agbaglah, L. Duchemin, & C Josserand, 2011, *[Phys.
    Fluids](http://scitation.aip.org/phf/)*, vol. 23, 122101.
-   Curvature singularity and film-skating during drop
    impact ([PDF](https://www.irphe.fr/~duchemin/PDF/dj1.pdf))  
    L. Duchemin & C. Josserand, 2011, *[Phys.
    Fluids](http://scitation.aip.org/phf/)*, vol. 23, 091701 (2011).
-   Shape and stability of axisymmetric levitated viscous
    drops ([PDF](https://www.irphe.fr/~duchemin/PDF/ltpd.pdf))  
    J.R. Lister, A. B. Thompson, A. Perriot & L. Duchemin, 2008, *[J.
    Fluid Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*,
    vol. 617, 167–185.
-   Long-time evolution of inviscid thin
    jets ([PDF](https://www.irphe.fr/~duchemin/PDF/d.pdf)) L. Duchemin,
    2008, *[Proc. R. Soc. A](http://rspa.royalsocietypublishing.org/)*,
    vol. 464, no 2089, 197–206.
-   Ablative Rayleigh-Taylor instability for strong temperature
    dependence of thermal
    conductivity ([PDF](https://www.irphe.fr/~duchemin/PDF/acds.pdf)) C.
    Almarcha, P. Clavin, L. Duchemin & J. Sanz, 2007, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    579, 481–492.
-   Asymptotic behaviour of the Rayleigh-Taylor
    instability ([PDF](https://www.irphe.fr/~duchemin/PDF/djc.pdf)) L.
    Duchemin, C. Josserand & P. Clavin, 2005, *[Phys. Rev.
    Lett.](http://prl.aps.org/)*, 94, 224501.
-   Static shapes of a levitated viscous
    drop ([PDF](https://www.irphe.fr/~duchemin/PDF/dll.pdf)) L.
    Duchemin, J. Lister & U. Lange, 2005, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    533, 161–170.
-   The effect of solid boundaries on pore shrinkage in Stokes
    flow ([PDF](https://www.irphe.fr/~duchemin/PDF/cd.pdf)) D. Crowdy
    & L. Duchemin, 2005, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    531, 359–379.
-   Inviscid coalescence of
    drops ([PDF](https://www.irphe.fr/~duchemin/PDF/dej.pdf)) L.
    Duchemin, J. Eggers & C. Josserand, 2003, *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, vol.
    487, 167–178.
-   Pyramidal and toroidal water drops after impact on a solid
    surface ([PDF](https://www.irphe.fr/~duchemin/PDF/Pyramids.pdf))
    Renardy, Y., Popinet, S., Duchemin, L., Renardy, M., Zaleski, S.,
    Josserand, C., Drumright-Clarke, M.A., Richard, D., Clanet, C.,
    Quéré, D. *[J. Fluid
    Mech.](http://128.232.233.5/action/displayJournal?jid=FLM)*, **484**,
    69–83 (2003).
-   Jet Formation in gas bubbles bursting at a free
    surface ([PDF](https://www.irphe.fr/~duchemin/PDF/dpjz.pdf))
    Duchemin, L., Popinet, S., Josserand, C., Zaleski, S. *[Phys.
    Fluids](http://scitation.aip.org/phf/)*, **14**, 3000 (2002).

