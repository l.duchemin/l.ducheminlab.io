---
pagetitle: Laurent Duchemin's Webpage
navbar_resume: true
---

# Career

-   Since 2005 : Assistant Professor, [Aix-Marseille
    Université](http://www.univ-amu.fr/), Marseille, France.
-   2003-2005 : Post-doctoral researcher,
    [DAMTP](http://www.damtp.cam.ac.uk/), Cambridge, UK.
-   2002-2003 : Post-doctoral researcher, [Imperial
    College](http://www3.imperial.ac.uk/), London, UK.
-   2001-2002 : Post-doctoral researcher, IFP, Rueil-Malmaison, France.
-   December 2001 : PhD, Université Aix-Marseille II.

# Past students

-   Lionel VINCENT : PhD defended in December 2013.

