---
pagetitle: page title for section 1
navbar_divers: true
---

<!----------------->

Section 1
=========
This is the first paragraph. 

This is the 2nd paragraph. 


Second level (h2) title
-----------------------

You can write italic and bold like this: *Italic*, **bold**. 

$$
\int_0^1 {x^2 dx} = \frac{1}{3}
$$

An example list: 

  * item 1
  * item 2
  * item 3

Here is a numbered list: 

  1. item 1
  2. item 2
  3. item 3

You can write code by enclosing them with three-tilde lines: 

~~~
int main(void) {
    print "Hello world!";
    return 0;
}
~~~

Syntax highlighting works for many languages (check [pandoc
doc](http://pandoc.org/README.html#syntax-highlighting)):

~~~c
int main(void) {
    print "Hello world!";
    return 0;
}
~~~



### An h3 header ###

An example link to [a website](http://foo.bar) and to a [local
page](local-doc.html). 


## What about tables?  ###
### What about tables?  ###

Here is an example

|col1|col2|
|-----|----|
|1|X      
|2|Y
|3|Z 

  Right     Left     Center     Default
-------     ------ ----------   -------
     12     12        12            12
    123     123       123          123
      1     1          1             1

+---------------+---------------+--------------------+
| Fruit         | Price         | Advantages         |
+===============+===============+====================+
| Bananas       | $1.34         | - built-in wrapper |
|               |               | - bright color     |
+---------------+---------------+--------------------+
| Oranges       | $2.10         | - cures scurvy     |
|               |               | - tasty            |
+---------------+---------------+--------------------+

Comprehensive guide to Parndoc's markdown is
[here](http://pandoc.org/README.html#pandocs-markdown).
