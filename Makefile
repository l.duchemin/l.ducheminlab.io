STRING1 = "Last updated on "
STRING2 = "Created using [pandoc]\(http://pandoc.org/\) and [TSPW]\(https://github.com/eakbas/TSPW\)."


RESOURCEDIR = "resources"
PAPERSDIR = "Papers"
VARIOUSDIR = "Various"
PICSDIR = "Pics"
TARGETDIR = "public"


SOURCES = $(wildcard *.md)
HTMLs = $(patsubst %.md,public/%.html,$(SOURCES))
TEMPFILE = public/sdfgsdfs7fs8d7tfgsduifgsdi5234j

all: mkdir copy_resources copy_pics copy_papers copy_various $(HTMLs)

mkdir:
	mkdir -p $(TARGETDIR)

copy_resources:
	cp -r $(RESOURCEDIR) $(TARGETDIR)

copy_pics:
	cp -r $(PICSDIR) $(TARGETDIR)

copy_papers:
	cp -r $(PAPERSDIR) $(TARGETDIR)

copy_various:
	cp -r $(VARIOUSDIR) $(TARGETDIR)

public/%.html: %.md
	cat $< > $(TEMPFILE)
	echo "\n***\n\n<span class="footer">*$(STRING1) `stat -c %Y Makefile  | date +'%b %d, %Y'`. $(STRING2)*</span>" >> $(TEMPFILE)
	pandoc --mathjax -t html5 -s -c $(RESOURCEDIR)/style.css --template=template.html --highlight-style kate --bibliography=coalescence.bib --citeproc --reference-links --reference-location=block $(TEMPFILE) -o $@
	rm -f $(TEMPFILE)

clean: 
	rm -rf $(TARGETDIR)

