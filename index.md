---
pagetitle: Laurent Duchemin's Webpage
navbar_home: true
---

<!----------------->

# Laurent Duchemin

<img src="resources/portrait_small.jpg" id="profile_pic"/>
<!--<img src="resources/profile_pic.png" id="profile_pic"/>-->


**Welcome to my webpage!**

I am a Professor at [ESPCI](https://www.espci.psl.eu/en/), and my research activity is carried out at [PMMH](https://www.pmmh.espci.fr/) laboratory. My research mainly deals with theoretical and numerical problems involving free surface flows. Usually, my approach consists in simplifying the equations in order to solve them with light fluid dynamics codes, like boundary integral methods, and confront theoretical models to the numerics. If you want more details about my recent research activity, please follow the [Research](research.html) link on top of this page. 

## Recent news

Please, checkout our last submitted paper on stiff partial differential equations ([PDF](Papers/Stiff.pdf)).
